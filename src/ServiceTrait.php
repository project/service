<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the service container.
 */
trait ServiceTrait {

  /**
   * The service container.
   */
  protected ContainerInterface $serviceModuleContainer;

  /**
   * The service objects.
   */
  protected array $serviceItems = [];

  /**
   * Sets a service that does not have a trait in this module.
   *
   * @param string $name
   *   The service name.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   * @param bool $override
   *   (optional) TRUE if service should be loaded even if it is done before.
   *   Defaults to FALSE.
   */
  protected function addService(
    string $name,
    ?ContainerInterface $container = NULL,
    bool $override = FALSE,
  ): static {
    if (!isset($this->serviceModuleContainer) && !isset($container)) {
      @trigger_error('Calling addService() required a second parameter because the service container is not defined yet.');
      return $this;
    }
    elseif (isset($container)) {
      $this->serviceModuleContainer = $container;
    }

    $parts = explode('.', $name);

    if (
      property_exists($this, $property = end($parts)) &&
      ($override || !isset($this->$property)) &&
      $this->serviceModuleContainer->has($name)
    ) {
      try {
        $this->$property = $this->serviceModuleContainer->get($name);
      }
      catch (\Exception $exception) {}
    }

    return $this;
  }

  /**
   * Gets service which has a trait in this module.
   *
   * @param string|null $id
   *   (optional) The service name. Defaults to NULL.
   */
  protected function getKnownService(?string $id = NULL): mixed {
    if (($data = $this->searchKnownService('getKnownService', $id)) !== NULL) {
      $id = $data['id'];

      if (isset($data['property'])) {
        $property = $data['property'];

        if (!isset($this->$property) && \Drupal::hasService($id)) {
          $this->$property = \Drupal::service($id);
        }

        return $this->$property;
      }
      else {
        if (!isset($this->serviceItems[$id]) && \Drupal::hasService($id)) {
          $this->serviceItems[$id] = \Drupal::service($id);
        }

        return $this->serviceItems[$id];
      }
    }

    return NULL;
  }

  /**
   * Search service which has a trait in this module.
   *
   * @param string $method
   *   The method name.
   * @param string|null $id
   *   (optional) The service name. Defaults to NULL.
   */
  protected function searchKnownService(
    string $method,
    ?string $id = NULL,
  ): ?array {
    if ($id !== NULL) {
      return ['id' => $id];
    }

    $items = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
    $index = array_search($method, array_column($items, 'function'));

    if (
      $index !== FALSE &&
      preg_match('#([^/]+)Trait\.php$#', $items[$index]['file'], $matches)
    ) {
      $data = ['id' => constant('static::service' . $matches[1])];

      if (property_exists($this, $property = lcfirst($matches[1]))) {
        $data['property'] = $property;
      }

      return $data;
    }

    return NULL;
  }

  /**
   * Sets service which has a trait in this module.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   * @param string|null $id
   *   (optional) The service name. Defaults to NULL.
   */
  protected function setKnownService(
    ?ContainerInterface $container = NULL,
    ?string $id = NULL,
  ): static {
    if (!isset($this->serviceModuleContainer) && !isset($container)) {
      @trigger_error('Calling setKnownService() required a parameter because the service container is not defined yet.');
      return $this;
    }
    elseif (isset($container)) {
      $this->serviceModuleContainer = $container;
    }

    if (($data = $this->searchKnownService('setKnownService', $id)) !== NULL) {
      $id = $data['id'];
      $added = FALSE;

      if (isset($data['property'])) {
        $property = $data['property'];

        if (
          !isset($this->$property) &&
          $this->serviceModuleContainer->has($id)
        ) {
          $added = TRUE;

          try {
            $this->$property = $this->serviceModuleContainer->get($id);
          }
          catch (\Exception $exception) {
            $added = FALSE;
          }
        }
      }

      if (
        !$added &&
        !isset($this->serviceItems[$id]) &&
        $this->serviceModuleContainer->has($id)
      ) {
        $this->serviceItems[$id] = $this->serviceModuleContainer->get($id);
      }
    }

    return $this;
  }

}
