<?php

namespace Drupal\service;

use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the cache service.
 */
trait CacheTrait {

  use ServiceTrait;

  /**
   * Sets the cache.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   * @param string $bin
   *   (optional) The cache name. Defaults to 'default'.
   */
  protected function addCache(
    ?ContainerInterface $container = NULL,
    string $bin = 'default'
  ): static {
    return $this->setKnownService($container, 'cache.' . $bin);
  }

  /**
   * Gets the cache.
   *
   * @param string $bin
   *   (optional) The cache bin for which the cache object should be returned.
   *   Defaults to 'default'.
   *
   * @see \Drupal::cache()
   * @see \Drupal\Core\Controller\ControllerBase::cache()
   */
  protected function cache($bin = 'default'): CacheBackendInterface {
    return $this->getKnownService('cache.' . $bin);
  }

}
