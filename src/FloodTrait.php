<?php

namespace Drupal\service;

use Drupal\Core\Flood\FloodInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the flood service.
 */
trait FloodTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFlood = 'flood';

  /**
   * Sets the flood.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFlood(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the flood.
   *
   * @see \Drupal::flood()
   */
  protected function flood(): FloodInterface {
    return $this->getKnownService();
  }

}
