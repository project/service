<?php

namespace Drupal\service;

use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the key/value store factory service.
 */
trait KeyValueTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceKeyValue = 'keyvalue';

  /**
   * Sets the key/value store factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addKeyValue(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the key/value storage collection.
   *
   * @param string $collection
   *   The name of the collection holding key and value pairs.
   *
   * @see \Drupal::keyValue()
   */
  protected function keyValue(string $collection): KeyValueStoreInterface {
    return $this->getKnownService()->get($collection);
  }

}
