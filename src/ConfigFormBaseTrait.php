<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base trait for implementing system configuration forms.
 */
trait ConfigFormBaseTrait {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return parent::create($container)->addContainer($container)->creation();
  }

}
