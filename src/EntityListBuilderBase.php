<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a generic implementation to build a listing of entities.
 */
abstract class EntityListBuilderBase extends EntityListBuilder {

  use EntityListBuilderBaseTrait;

}
