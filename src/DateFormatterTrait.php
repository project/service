<?php

namespace Drupal\service;

use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the date formatter service.
 */
trait DateFormatterTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceDateFormatter = 'date.formatter';

  /**
   * Sets the date formatter.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addDateFormatter(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the date formatter.
   */
  protected function dateFormatter(): DateFormatterInterface {
    return $this->getKnownService();
  }

}
