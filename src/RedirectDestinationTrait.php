<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the redirect destination service.
 *
 * @see \Drupal\Core\Routing\RedirectDestinationTrait
 */
trait RedirectDestinationTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceRedirectDestination = 'redirect.destination';

  /**
   * Sets the redirect destination.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addRedirectDestination(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Returns the redirect destination service.
   *
   * @return \Drupal\Core\Routing\RedirectDestinationInterface
   *   The redirect destination helper.
   *
   * @see \Drupal::destination()
   */
  protected function getRedirectDestination() {
    return $this->getKnownService();
  }

}
