<?php

namespace Drupal\service;

use Drupal\Core\TypedData\TypedDataManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the typed data manager service.
 */
trait TypedDataManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTypedDataManager = 'typed_data_manager';

  /**
   * Sets the typed data manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTypedDataManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the typed data manager.
   */
  protected function typedDataManager(): TypedDataManagerInterface {
    return $this->getKnownService();
  }

}
