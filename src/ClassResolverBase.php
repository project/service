<?php

namespace Drupal\service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base trait for implementing the class resolver interface.
 */
abstract class ClassResolverBase implements ContainerInjectionInterface {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return (new static())->addContainer($container)->creation();
  }

}
