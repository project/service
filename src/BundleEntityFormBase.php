<?php

namespace Drupal\service;

use Drupal\Core\Entity\BundleEntityFormBase as CoreBundleEntityFormBase;

/**
 * Class BundleEntityFormBase is a base form for bundle config entities.
 */
abstract class BundleEntityFormBase extends CoreBundleEntityFormBase {

  use FormBaseTrait;

}
