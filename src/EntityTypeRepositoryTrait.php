<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity type repository service.
 */
trait EntityTypeRepositoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityTypeRepository = 'entity_type.repository';

  /**
   * Sets the entity type repository.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityTypeRepository(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity type repository.
   */
  protected function entityTypeRepository(): EntityTypeRepositoryInterface {
    return $this->getKnownService();
  }

}
