<?php

namespace Drupal\service;

use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the database connection service.
 */
trait ConnectionTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceConnection = 'database';

  /**
   * The database connection.
   *
   * Prevents the database connection from being serialized.
   *
   * @see \Drupal\Core\Database\Connection::__sleep()
   */
  protected Connection $connection;

  /**
   * Sets the database connection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addConnection(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the database connection based on the service class name.
   */
  protected function connection(): Connection {
    return $this->getKnownService();
  }

  /**
   * Gets the database connection based on the service identifier.
   *
   * @see \Drupal::database()
   */
  protected function database(): Connection {
    return $this->connection();
  }

}
