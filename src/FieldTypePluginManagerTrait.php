<?php

namespace Drupal\service;

use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the field type plugin manager service.
 */
trait FieldTypePluginManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFieldTypePluginManager = 'plugin.manager.field.field_type';

  /**
   * Sets the field type plugin manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFieldTypePluginManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the field type plugin manager.
   */
  protected function fieldTypePluginManager(): FieldTypePluginManagerInterface {
    return $this->getKnownService();
  }

}
