<?php

namespace Drupal\service;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the file URL generator service.
 */
trait FileUrlGeneratorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFileUrlGenerator = 'file_url_generator';

  /**
   * Sets the file URL generator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFileUrlGenerator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the file URL generator.
   */
  protected function fileUrlGenerator(): FileUrlGeneratorInterface {
    return $this->getKnownService();
  }

}
