<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a trait for the request stack service.
 */
trait RequestStackTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceRequestStack = 'request_stack';

  /**
   * Sets the request stack.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addRequestStack(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the request stack.
   *
   * @see \Drupal::requestStack()
   */
  protected function requestStack(): RequestStack {
    return $this->getKnownService();
  }

}
