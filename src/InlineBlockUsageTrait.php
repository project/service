<?php

namespace Drupal\service;

use Drupal\layout_builder\InlineBlockUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the inline block usage tracking service.
 */
trait InlineBlockUsageTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceInlineBlockUsage = 'inline_block.usage';

  /**
   * Sets the inline block usage tracking.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addInlineBlockUsage(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the inline block usage tracking.
   */
  protected function inlineBlockUsage(): InlineBlockUsageInterface {
    return $this->getKnownService();
  }

}
