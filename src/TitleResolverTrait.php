<?php

namespace Drupal\service;

use Drupal\Core\Controller\TitleResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the title resolver service.
 */
trait TitleResolverTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTitleResolver = 'title_resolver';

  /**
   * Sets the title resolver.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTitleResolver(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the title resolver.
   */
  protected function titleResolver(): TitleResolverInterface {
    return $this->getKnownService();
  }

}
