<?php

namespace Drupal\service;

use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the user data service.
 */
trait UserDataTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceUserData = 'user.data';

  /**
   * Sets the user data.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addUserData(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the user data.
   */
  protected function userData(): UserDataInterface {
    return $this->getKnownService();
  }

}
