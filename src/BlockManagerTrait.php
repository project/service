<?php

namespace Drupal\service;

use Drupal\Core\Block\BlockManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the block manager service.
 */
trait BlockManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceBlockManager = 'plugin.manager.block';

  /**
   * Sets the block manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addBlockManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the block manager.
   */
  protected function blockManager(): BlockManagerInterface {
    return $this->getKnownService();
  }

}
