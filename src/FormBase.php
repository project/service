<?php

namespace Drupal\service;

use Drupal\Core\Form\FormBase as CoreFormBase;

/**
 * Provides a base class for forms.
 */
abstract class FormBase extends CoreFormBase {

  use FormBaseTrait;

}
