<?php

namespace Drupal\service;

use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the mail manager service.
 */
trait MailManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMailManager = 'plugin.manager.mail';

  /**
   * Sets the mail manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMailManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the mail manager.
   */
  protected function mailManager(): MailManagerInterface {
    return $this->getKnownService();
  }

}
