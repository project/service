<?php

namespace Drupal\service;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\symfony_mailer\Processor\EmailAdjusterBase as OriginalEmailAdjusterBase;

/**
 * Defines the base class for EmailAdjuster plug-ins.
 *
 * @see https://www.drupal.org/project/symfony_mailer
 */
abstract class EmailAdjusterBase extends OriginalEmailAdjusterBase implements ContainerFactoryPluginInterface {

  use PluginBaseTrait;

}
