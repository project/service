<?php

namespace Drupal\service;

use Drupal\Core\DrupalKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the Drupal kernel service.
 */
trait KernelTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceKernel = 'kernel';

  /**
   * Sets the Drupal kernel.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addKernel(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the Drupal kernel.
   */
  protected function kernel(): DrupalKernelInterface {
    return $this->getKnownService();
  }

}
