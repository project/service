<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity repository service.
 */
trait EntityRepositoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityRepository = 'entity.repository';

  /**
   * Sets the entity repository.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityRepository(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity repository.
   */
  protected function entityRepository(): EntityRepositoryInterface {
    return $this->getKnownService();
  }

}
