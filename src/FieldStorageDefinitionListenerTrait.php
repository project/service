<?php

namespace Drupal\service;

use Drupal\Core\Field\FieldStorageDefinitionListenerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the field storage definition listener service.
 */
trait FieldStorageDefinitionListenerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFieldStorageDefinitionListener = 'field_storage_definition.listener';

  /**
   * Sets the field storage definition listener.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFieldStorageDefinitionListener(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the field storage definition listener.
   */
  protected function fieldStorageDefinitionListener(): FieldStorageDefinitionListenerInterface {
    return $this->getKnownService();
  }

}
