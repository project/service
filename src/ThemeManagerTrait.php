<?php

namespace Drupal\service;

use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the theme manager service.
 */
trait ThemeManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceThemeManager = 'theme.manager';

  /**
   * Sets the theme manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addThemeManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the theme manager.
   *
   * @see \Drupal::theme()
   */
  protected function themeManager(): ThemeManagerInterface {
    return $this->getKnownService();
  }

}
