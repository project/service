<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a generic implementation to build a listing of entities.
 */
trait EntityListBuilderBaseTrait {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type,
  ): static {
    return parent::createInstance($container, $entity_type)
      ->addContainer($container)
      ->creation();
  }

}
