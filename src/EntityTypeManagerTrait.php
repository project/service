<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity type manager service.
 */
trait EntityTypeManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityTypeManager = 'entity_type.manager';

  /**
   * Sets the entity type manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityTypeManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity type manager.
   *
   * @see \Drupal::entityTypeManager()
   */
  protected function entityTypeManager(): EntityTypeManagerInterface {
    return $this->getKnownService();
  }

}
