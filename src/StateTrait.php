<?php

namespace Drupal\service;

use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the state service.
 */
trait StateTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceState = 'state';

  /**
   * Sets the state.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addState(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the state.
   *
   * @see \Drupal::state()
   */
  protected function state(): StateInterface {
    return $this->getKnownService();
  }

}
