<?php

namespace Drupal\service;

use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the file repository service.
 */
trait FileRepositoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFileRepository = 'file.repository';

  /**
   * Sets the file repository.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFileRepository(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the file repository.
   */
  protected function fileRepository(): FileRepositoryInterface {
    return $this->getKnownService();
  }

}
