<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the string translation service.
 *
 * @see \Drupal\Core\StringTranslation\StringTranslationTrait
 */
trait StringTranslationTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceStringTranslation = 'string_translation';

  /**
   * Sets the string translation service to use.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addStringTranslation(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the string translation service.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface
   *   The string translation service.
   *
   * @see \Drupal::translation()
   */
  protected function getStringTranslation() {
    return $this->getKnownService();
  }

}
