<?php

namespace Drupal\service;

use Drupal\Core\Block\BlockBase as CoreBlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines a base block implementation that most blocks plugins will extend.
 */
abstract class BlockBase extends CoreBlockBase implements ContainerFactoryPluginInterface {

  use PluginBaseTrait;

}
