<?php

namespace Drupal\service;

use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the URL generator service.
 */
trait UrlGeneratorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceUrlGenerator = 'url_generator';

  /**
   * Sets the URL generator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addUrlGenerator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the URL generator.
   *
   * @see \Drupal::urlGenerator()
   */
  protected function urlGenerator(): UrlGeneratorInterface {
    return $this->getKnownService();
  }

}
