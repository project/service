<?php

namespace Drupal\service;

use Drupal\bootstrap\Plugin\Preprocess\PreprocessBase as OriginalPreprocessBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Base preprocess class used to build the necessary variables for templates.
 *
 * @see https://www.drupal.org/project/bootstrap
 */
abstract class PreprocessBase extends OriginalPreprocessBase implements ContainerFactoryPluginInterface {

  use PluginBaseTrait;

}
