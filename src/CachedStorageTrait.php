<?php

namespace Drupal\service;

use Drupal\Core\Config\StorageCacheInterface;
use Drupal\Core\Config\StorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the config storage service.
 */
trait CachedStorageTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceCachedStorage = 'config.storage';

  /**
   * Sets the config storage.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addCachedStorage(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the config storage.
   */
  protected function configStorage(): StorageInterface&StorageCacheInterface {
    return $this->getKnownService();
  }

}
