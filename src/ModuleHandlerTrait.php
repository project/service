<?php

namespace Drupal\service;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the module handler service.
 */
trait ModuleHandlerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceModuleHandler = 'module_handler';

  /**
   * Sets the module handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addModuleHandler(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the module handler.
   *
   * @see \Drupal::moduleHandler()
   */
  protected function moduleHandler(): ModuleHandlerInterface {
    return $this->getKnownService();
  }

}
