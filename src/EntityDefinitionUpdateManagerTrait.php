<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity definition update manager service.
 */
trait EntityDefinitionUpdateManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityDefinitionUpdateManager = 'entity.definition_update_manager';

  /**
   * Sets the entity definition update manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityDefinitionUpdateManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity definition update manager.
   *
   * @see \Drupal::entityDefinitionUpdateManager()
   */
  protected function entityDefinitionUpdateManager(): EntityDefinitionUpdateManagerInterface {
    return $this->getKnownService();
  }

}
