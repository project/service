<?php

namespace Drupal\service;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the typed configuration manager service.
 */
trait TypedConfigManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTypedConfigManager = 'config.typed';

  /**
   * Sets the typed configuration manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTypedConfigManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the typed configuration manager.
   */
  protected function typedConfigManager(): TypedConfigManagerInterface {
    return $this->getKnownService();
  }

}
