<?php

namespace Drupal\service;

use Drupal\Core\Controller\ControllerBase as CoreControllerBase;

/**
 * Utility base class for thin controllers.
 */
abstract class ControllerBase extends CoreControllerBase {

  use ControllerBaseTrait;

}
