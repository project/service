<?php

namespace Drupal\service;

use Drupal\Core\Path\PathValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the path validator service.
 */
trait PathValidatorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string servicePathValidator = 'path.validator';

  /**
   * Sets the path validator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addPathValidator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the path validator.
   *
   * @see \Drupal::pathValidator()
   */
  protected function pathValidator(): PathValidatorInterface {
    return $this->getKnownService();
  }

}
