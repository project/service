<?php

namespace Drupal\service;

use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the path alias manager service.
 */
trait AliasManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceAliasManager = 'path_alias.manager';

  /**
   * Sets the path alias manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addAliasManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the path alias manager.
   */
  protected function aliasManager(): AliasManagerInterface {
    return $this->getKnownService();
  }

}
