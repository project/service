<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Utility base trait for thin controllers.
 */
trait ControllerBaseTrait {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return parent::create($container)->addContainer($container)->creation();
  }

}
