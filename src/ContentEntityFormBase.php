<?php

namespace Drupal\service;

use Drupal\Core\Entity\ContentEntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity form variant for content entity types.
 */
abstract class ContentEntityFormBase extends ContentEntityForm {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return parent::create($container)->addContainer($container)->creation();
  }

}
