<?php

namespace Drupal\service;

use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the form builder service.
 */
trait FormBuilderTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFormBuilder = 'form_builder';

  /**
   * Sets the form builder.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFormBuilder(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the form builder.
   *
   * @see \Drupal::formBuilder()
   */
  protected function formBuilder(): FormBuilderInterface {
    return $this->getKnownService();
  }

}
