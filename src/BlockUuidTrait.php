<?php

namespace Drupal\service;

use Drupal\block_content\BlockContentUuidLookup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the block content UUID lookup service.
 */
trait BlockUuidTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceBlockUuid = 'block_content.uuid_lookup';

  /**
   * Sets the block content UUID lookup.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addBlockUuid(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the block content UUID lookup.
   */
  protected function blockUuid(): BlockContentUuidLookup {
    return $this->getKnownService();
  }

}
