<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity display repository service.
 */
trait EntityDisplayRepositoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityDisplayRepository = 'entity_display.repository';

  /**
   * Sets the entity display repository.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityDisplayRepository(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity display repository.
   */
  protected function entityDisplayRepository(): EntityDisplayRepositoryInterface {
    return $this->getKnownService();
  }

}
