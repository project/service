<?php

namespace Drupal\service;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Defines the default class to build a listing of configuration entities.
 */
abstract class ConfigEntityListBuilderBase extends ConfigEntityListBuilder {

  use EntityListBuilderBaseTrait;

}
