<?php

namespace Drupal\service;

use Drupal\Core\Form\ConfigFormBase as CoreConfigFormBase;

/**
 * Base class for implementing system configuration forms.
 */
abstract class ConfigFormBase extends CoreConfigFormBase {

  use ConfigFormBaseTrait;

}
