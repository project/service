<?php

namespace Drupal\service;

use Drupal\Component\Serialization\SerializationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the json serialization service.
 */
trait JsonSerializationTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceJsonSerialization = 'serialization.json';

  /**
   * Sets the json serialization.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addJsonSerialization(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the json serialization.
   */
  protected function jsonSerialization(): SerializationInterface {
    return $this->getKnownService();
  }

}
