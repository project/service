<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a trait for the event dispatcher service.
 */
trait EventDispatcherTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEventDispatcher = 'event_dispatcher';

  /**
   * Sets the event dispatcher.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEventDispatcher(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the event dispatcher.
   */
  protected function eventDispatcher(): EventDispatcherInterface {
    return $this->getKnownService();
  }

}
