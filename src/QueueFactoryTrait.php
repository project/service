<?php

namespace Drupal\service;

use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the queue factory service.
 */
trait QueueFactoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceQueueFactory = 'queue';

  /**
   * Sets the queue factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addQueueFactory(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the queue factory.
   *
   * @see \Drupal::queue()
   */
  protected function queueFactory(): QueueFactory {
    return $this->getKnownService();
  }

}
