<?php

namespace Drupal\service;

use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the views executable factory service.
 */
trait ExecutableFactoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceExecutableFactory = 'views.executable';

  /**
   * Sets the views executable factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addExecutableFactory(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the views executable factory.
   */
  protected function executableFactory(): ViewExecutableFactory {
    return $this->getKnownService();
  }

}
