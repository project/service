<?php

namespace Drupal\service;

use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the router builder service.
 */
trait RouteBuilderTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceRouteBuilder = 'router.builder';

  /**
   * Sets the router builder.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addRouteBuilder(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the router builder.
   */
  protected function routerBuilder(): RouteBuilderInterface {
    return $this->getKnownService();
  }

}
