<?php

namespace Drupal\service;

use Drupal\Core\Extension\ThemeExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the theme extension list service.
 */
trait ThemeListTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceThemeList = 'extension.list.theme';

  /**
   * Sets the theme extension list.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addThemeList(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the theme extension list.
   */
  protected function themeList(): ThemeExtensionList {
    return $this->getKnownService();
  }

}
