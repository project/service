<?php

namespace Drupal\service;

use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the time service.
 */
trait TimeTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTime = 'datetime.time';

  /**
   * Sets the time.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTime(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the time.
   *
   * @see \Drupal::time()
   */
  protected function time(): TimeInterface {
    return $this->getKnownService();
  }

}
