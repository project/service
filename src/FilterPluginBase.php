<?php

namespace Drupal\service;

use Drupal\views\Plugin\views\filter\FilterPluginBase as CoreFilterPluginBase;

/**
 * Base class for Views filters handler plugins.
 */
abstract class FilterPluginBase extends CoreFilterPluginBase {

  use PluginBaseChildTrait;

}
