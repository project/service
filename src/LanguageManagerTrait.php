<?php

namespace Drupal\service;

use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the language manager service.
 */
trait LanguageManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceLanguageManager = 'language_manager';

  /**
   * Sets the language manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addLanguageManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the language manager.
   *
   * @see \Drupal::languageManager()
   */
  protected function languageManager(): LanguageManagerInterface {
    return $this->getKnownService();
  }

}
