<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity field manager service.
 */
trait EntityFieldManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityFieldManager = 'entity_field.manager';

  /**
   * Sets the entity field manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityFieldManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity field manager.
   */
  protected function entityFieldManager(): EntityFieldManagerInterface {
    return $this->getKnownService();
  }

}
