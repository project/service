<?php

namespace Drupal\service;

use Drupal\Core\Locale\CountryManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the country manager service.
 */
trait CountryManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceCountryManager = 'country_manager';

  /**
   * Sets the country manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addCountryManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the country manager.
   */
  protected function countryManager(): CountryManagerInterface {
    return $this->getKnownService();
  }

}
