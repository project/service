<?php

namespace Drupal\service;

use Drupal\Component\Uuid\UuidInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the UUID service.
 */
trait UuidTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceUuid = 'uuid';

  /**
   * Sets the UUID.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addUuid(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the UUID.
   */
  protected function uuid(): UuidInterface {
    return $this->getKnownService();
  }

}
