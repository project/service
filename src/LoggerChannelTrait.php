<?php

namespace Drupal\service;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the logger factory service.
 *
 * @see \Drupal\Core\Logger\LoggerChannelTrait
 */
trait LoggerChannelTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceLoggerChannel = 'logger.factory';

  /**
   * Sets the logger factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addLoggerChannel(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the logger for a specific channel.
   *
   * @param string $channel
   *   The name of the channel. Can be any string, but the general practice is
   *   to use the name of the subsystem calling this.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger for the given channel.
   *
   * @see \Drupal::logger()
   */
  protected function getLogger($channel) {
    $service = $this->getKnownService();

    return $service instanceof LoggerChannelFactoryInterface
      ? $service->get($channel) : \Drupal::logger($channel);
  }

}
