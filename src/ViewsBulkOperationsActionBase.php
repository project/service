<?php

namespace Drupal\service;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase as OriginalViewsBulkOperationsActionBase;

/**
 * Views Bulk Operations action plugin base.
 *
 * @see https://www.drupal.org/project/views_bulk_operations
 */
abstract class ViewsBulkOperationsActionBase extends OriginalViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  use PluginBaseTrait;

}
