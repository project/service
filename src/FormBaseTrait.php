<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base trait for forms.
 */
trait FormBaseTrait {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return parent::create($container)->addContainer($container)->creation();
  }

}
