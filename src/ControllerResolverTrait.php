<?php

namespace Drupal\service;

use Drupal\Core\Controller\ControllerResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the controller resolver service.
 */
trait ControllerResolverTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceControllerResolver = 'controller_resolver';

  /**
   * Sets the controller resolver.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addControllerResolver(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the controller resolver.
   */
  protected function controllerResolver(): ControllerResolverInterface {
    return $this->getKnownService();
  }

}
