<?php

namespace Drupal\service;

use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the views join plugin manager service.
 */
trait JoinManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceJoinManager = 'plugin.manager.views.join';

  /**
   * Sets the views join plugin manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addJoinManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the views join plugin manager.
   */
  protected function joinManager(): ViewsHandlerManager {
    return $this->getKnownService();
  }

}
