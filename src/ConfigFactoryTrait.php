<?php

namespace Drupal\service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the configuration factory service.
 */
trait ConfigFactoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceConfigFactory = 'config.factory';

  /**
   * Sets the configuration factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addConfigFactory(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the configuration factory.
   *
   * @see \Drupal::configFactory()
   */
  protected function configFactory(): ConfigFactoryInterface {
    return $this->getKnownService();
  }

}
