<?php

namespace Drupal\service;

use Drupal\Core\Routing\AdminContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the admin context service.
 */
trait AdminContextTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceAdminContext = 'router.admin_context';

  /**
   * Sets the admin context.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addAdminContext(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the admin context.
   */
  protected function adminContext(): AdminContext {
    return $this->getKnownService();
  }

}
