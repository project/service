<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the last installed schema repository service.
 */
trait SchemaRepositoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceSchemaRepository = 'entity.last_installed_schema.repository';

  /**
   * Sets the last installed schema repository.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addSchemaRepository(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the last installed schema repository.
   */
  protected function schemaRepository(): EntityLastInstalledSchemaRepositoryInterface {
    return $this->getKnownService();
  }

}
