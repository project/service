<?php

namespace Drupal\service;

use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the current active user service.
 */
trait CurrentUserTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceCurrentUser = 'current_user';

  /**
   * Sets the current active user.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addCurrentUser(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the current active user.
   *
   * @see \Drupal::currentUser()
   */
  protected function currentUser(): AccountProxyInterface {
    return $this->getKnownService();
  }

}
