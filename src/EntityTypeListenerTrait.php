<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityTypeListenerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity type listener service.
 */
trait EntityTypeListenerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityTypeListener = 'entity_type.listener';

  /**
   * Sets the entity type listener.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityTypeListener(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity type listener.
   */
  protected function entityTypeListener(): EntityTypeListenerInterface {
    return $this->getKnownService();
  }

}
