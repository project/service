<?php

namespace Drupal\service;

use Drupal\Core\Extension\ExtensionPathResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the extension path resolver service.
 */
trait ExtensionPathResolverTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceExtensionPathResolver = 'extension.path.resolver';

  /**
   * Sets the extension path resolver.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addExtensionPathResolver(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the extension path resolver.
   */
  protected function extensionPathResolver(): ExtensionPathResolver {
    return $this->getKnownService();
  }

}
