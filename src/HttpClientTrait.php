<?php

namespace Drupal\service;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the HTTP client service.
 */
trait HttpClientTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceHttpClient = 'http_client';

  /**
   * Sets the HTTP client.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addHttpClient(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the HTTP client.
   *
   * @see \Drupal::httpClient()
   */
  protected function httpClient(): ClientInterface {
    return $this->getKnownService();
  }

}
