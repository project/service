<?php

namespace Drupal\service;

use Drupal\Core\Transliteration\PhpTransliteration;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the transliteration service.
 */
trait TransliterationTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTransliteration = 'transliteration';

  /**
   * Sets the transliteration.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTransliteration(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the transliteration.
   *
   * @see \Drupal::transliteration()
   */
  protected function transliteration(): PhpTransliteration {
    return $this->getKnownService();
  }

}
