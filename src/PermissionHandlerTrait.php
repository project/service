<?php

namespace Drupal\service;

use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the permission handler service.
 */
trait PermissionHandlerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string servicePermissionHandler = 'user.permissions';

  /**
   * Sets the permission handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addPermissionHandler(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the permission handler.
   */
  protected function permissionHandler(): PermissionHandlerInterface {
    return $this->getKnownService();
  }

}
