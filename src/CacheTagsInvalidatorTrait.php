<?php

namespace Drupal\service;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the cache tags invalidator service.
 */
trait CacheTagsInvalidatorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceCacheTagsInvalidator = 'cache_tags.invalidator';

  /**
   * Sets the cache tags invalidator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addCacheTagsInvalidator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the cache tags invalidator.
   */
  protected function cacheTagsInvalidator(): CacheTagsInvalidatorInterface {
    return $this->getKnownService();
  }

}
