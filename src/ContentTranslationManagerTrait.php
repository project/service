<?php

namespace Drupal\service;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the content translation manager service.
 */
trait ContentTranslationManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceContentTranslationManager = 'content_translation.manager';

  /**
   * Sets the content translation manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addContentTranslationManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the content translation manager.
   */
  protected function contentTranslationManager(): ContentTranslationManagerInterface {
    return $this->getKnownService();
  }

}
