<?php

namespace Drupal\service;

use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the module extension list service.
 */
trait ModuleListTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceModuleList = 'extension.list.module';

  /**
   * Sets the module extension list.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addModuleList(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the module extension list.
   */
  protected function moduleList(): ModuleExtensionList {
    return $this->getKnownService();
  }

}
