<?php

namespace Drupal\service;

use Drupal\Core\Entity\Query\QueryFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the query factory service.
 */
trait QueryFactoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceQueryFactory = 'entity.query.sql';

  /**
   * Sets the query factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addQueryFactory(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the query factory.
   */
  protected function queryFactory(): QueryFactoryInterface {
    return $this->getKnownService();
  }

}
