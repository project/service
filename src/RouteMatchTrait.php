<?php

namespace Drupal\service;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the current route match service.
 */
trait RouteMatchTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceRouteMatch = 'current_route_match';

  /**
   * Sets the current route match.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addRouteMatch(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the current route match.
   *
   * @see \Drupal::routeMatch()
   */
  protected function routeMatch(): RouteMatchInterface {
    return $this->getKnownService();
  }

}
