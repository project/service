<?php

namespace Drupal\service;

use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the twig service.
 */
trait TwigTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTwig = 'twig';

  /**
   * Sets the twig.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTwig(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the twig.
   */
  protected function twig(): TwigEnvironment {
    return $this->getKnownService();
  }

}
