<?php

namespace Drupal\service;

use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the file system service.
 */
trait FileSystemTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFileSystem = 'file_system';

  /**
   * Sets the file system.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFileSystem(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the file system.
   */
  protected function fileSystem(): FileSystemInterface {
    return $this->getKnownService();
  }

}
