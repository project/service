<?php

namespace Drupal\service;

use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the current path service.
 */
trait CurrentPathTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceCurrentPath = 'path.current';

  /**
   * Sets the current path.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addCurrentPath(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the current path.
   */
  protected function currentPath(): CurrentPathStack {
    return $this->getKnownService();
  }

}
