<?php

namespace Drupal\service;

use Drupal\Core\Image\ImageFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the image factory service.
 */
trait ImageFactoryTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceImageFactory = 'image.factory';

  /**
   * Sets the image factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addImageFactory(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the image factory.
   */
  protected function imageFactory(): ImageFactory {
    return $this->getKnownService();
  }

}
