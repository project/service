<?php

namespace Drupal\service;

use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the tempstore factory service.
 */
trait TempStoreTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceTempStore = 'tempstore.private';

  /**
   * Sets the tempstore factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addTempStore(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the tempstore factory.
   */
  protected function tempStore(): PrivateTempStoreFactory {
    return $this->getKnownService();
  }

}
