<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for plugins supporting metadata inspection and translation.
 */
trait PluginBaseChildTrait {

  use ServiceBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    return parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    )->addContainer($container)->creation();
  }

}
