<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityFormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity form builder service.
 */
trait EntityFormBuilderTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityFormBuilder = 'entity.form_builder';

  /**
   * Sets the entity form builder.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityFormBuilder(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity form builder.
   */
  protected function entityFormBuilder(): EntityFormBuilderInterface {
    return $this->getKnownService();
  }

}
