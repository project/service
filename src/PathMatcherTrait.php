<?php

namespace Drupal\service;

use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the path matcher service.
 */
trait PathMatcherTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string servicePathMatcher = 'path.matcher';

  /**
   * Sets the path matcher.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addPathMatcher(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the path matcher.
   */
  protected function pathMatcher(): PathMatcherInterface {
    return $this->getKnownService();
  }

}
