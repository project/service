<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the messenger service.
 *
 * @see \Drupal\Core\Messenger\MessengerTrait
 */
trait MessengerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMessenger = 'messenger';

  /**
   * Sets the messenger.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMessenger(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the messenger.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   The messenger.
   *
   * @see \Drupal::messenger()
   */
  public function messenger() {
    return $this->getKnownService();
  }

}
