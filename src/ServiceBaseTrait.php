<?php

namespace Drupal\service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for abstractions.
 */
trait ServiceBaseTrait {

  use ServiceTrait;

  /**
   * Sets the container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   */
  protected function addContainer(ContainerInterface $container): static {
    $this->serviceModuleContainer = $container;

    return $this;
  }

  /**
   * Injects services.
   */
  abstract protected function creation(): static;

}
