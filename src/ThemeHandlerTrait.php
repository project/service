<?php

namespace Drupal\service;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the theme handler service.
 */
trait ThemeHandlerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceThemeHandler = 'theme_handler';

  /**
   * Sets the theme handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addThemeHandler(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the theme handler.
   */
  protected function themeHandler(): ThemeHandlerInterface {
    return $this->getKnownService();
  }

}
