<?php

namespace Drupal\service;

use Drupal\Core\Extension\ThemeExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the profile extension list service.
 */
trait ProfileListTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceProfileList = 'extension.list.profile';

  /**
   * Sets the profile extension list.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addProfileList(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the profile extension list.
   */
  protected function profileList(): ThemeExtensionList {
    return $this->getKnownService();
  }

}
