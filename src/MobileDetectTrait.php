<?php

namespace Drupal\service;

use Detection\MobileDetect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the mobile detect service.
 *
 * @see https://www.drupal.org/project/mobile_detect
 */
trait MobileDetectTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMobileDetect = 'mobile_detect';

  /**
   * Sets the mobile detect.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMobileDetect(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the mobile detect.
   */
  protected function mobileDetect(): MobileDetect {
    return $this->getKnownService();
  }

}
