<?php

namespace Drupal\service;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the entity type bundle info service.
 */
trait EntityTypeBundleInfoTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEntityTypeBundleInfo = 'entity_type.bundle.info';

  /**
   * Sets the entity type bundle info.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEntityTypeBundleInfo(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the entity type bundle info.
   */
  protected function entityTypeBundleInfo(): EntityTypeBundleInfoInterface {
    return $this->getKnownService();
  }

}
