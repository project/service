<?php

namespace Drupal\service;

use Drupal\Core\Extension\ModuleInstallerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the module installer service.
 */
trait ModuleInstallerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceModuleInstaller = 'module_installer';

  /**
   * Sets the module installer.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addModuleInstaller(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the module installer.
   */
  protected function moduleInstaller(): ModuleInstallerInterface {
    return $this->getKnownService();
  }

}
