<?php

namespace Drupal\service;

use Drupal\Core\Field\WidgetBase as CoreWidgetBase;

/**
 * Base class for 'Field widget' plugin implementations.
 */
abstract class WidgetBase extends CoreWidgetBase {

  use PluginBaseChildTrait;

}
