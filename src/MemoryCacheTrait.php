<?php

namespace Drupal\service;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the memory cache service.
 */
trait MemoryCacheTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMemoryCache = 'entity.memory_cache';

  /**
   * Sets the memory cache.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMemoryCache(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the memory cache.
   */
  protected function memoryCache(): MemoryCacheInterface {
    return $this->getKnownService();
  }

}
