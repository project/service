<?php

namespace Drupal\service;

use Drupal\Core\Extension\ThemeInstallerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the theme installer service.
 */
trait ThemeInstallerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceThemeInstaller = 'theme_installer';

  /**
   * Sets the theme installer.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addThemeInstaller(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the theme installer.
   */
  protected function themeInstaller(): ThemeInstallerInterface {
    return $this->getKnownService();
  }

}
