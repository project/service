<?php

namespace Drupal\service;

use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the renderer service.
 */
trait RendererTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceRenderer = 'renderer';

  /**
   * Sets the renderer.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addRenderer(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the renderer.
   */
  protected function renderer(): RendererInterface {
    return $this->getKnownService();
  }

}
