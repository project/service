<?php

namespace Drupal\service;

use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the token service.
 */
trait TokenTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceToken = 'token';

  /**
   * Sets the token.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addToken(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the token.
   *
   * @see \Drupal::token()
   */
  protected function token(): Token {
    return $this->getKnownService();
  }

}
