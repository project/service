<?php

namespace Drupal\service;

use Drupal\Core\Menu\MenuLinkTreeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the menu link tree service.
 */
trait MenuTreeTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMenuTree = 'menu.link_tree';

  /**
   * Sets the menu link tree.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMenuTree(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the menu link tree.
   *
   * @see \Drupal::menuTree()
   */
  protected function menuTree(): MenuLinkTreeInterface {
    return $this->getKnownService();
  }

}
