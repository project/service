<?php

namespace Drupal\service;

use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the migration plugin manager service.
 */
trait MigrationManagerTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceMigrationManager = 'plugin.manager.migration';

  /**
   * Sets the migration plugin manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addMigrationManager(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the migration plugin manager.
   */
  protected function migrationManager(): MigrationPluginManagerInterface {
    return $this->getKnownService();
  }

}
