<?php

namespace Drupal\service;

use Drupal\Component\Utility\EmailValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the E-mail validator service.
 */
trait EmailValidatorTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceEmailValidator = 'email.validator';

  /**
   * Sets the E-mail validator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addEmailValidator(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the E-mail validator.
   */
  protected function emailValidator(): EmailValidatorInterface {
    return $this->getKnownService();
  }

}
